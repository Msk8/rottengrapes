from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

from .views import (
    MovieList,
    MovieDetail,
    MovieCreation,
    MovieUpdate,
    MovieDelete
)

urlpatterns = [

    url(r'^$', MovieList.as_view(), name='list'),
    url(r'^(?P<pk>\d+)$', MovieDetail.as_view(), name='detail'),
    url(r'^new$', MovieCreation.as_view(), name='new'),
    url(r'^edit/(?P<pk>\d+)$', MovieUpdate.as_view(), name='edit'),
    url(r'^delete/(?P<pk>\d+)$', MovieDelete.as_view(), name='delete'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)