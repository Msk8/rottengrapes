from django import forms
from .models import *

class DateInput(forms.DateInput):
	input_type = 'date'

class MovieForm(forms.ModelForm):
	class Meta:
		model = Movie
		fields = '__all__'
		widgets = {
			'release_date': DateInput()
		}