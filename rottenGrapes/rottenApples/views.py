# # -*- coding: utf-8 -*-
# from __future__ import unicode_literals

# from django.shortcuts import render

# # Create your views here.
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (CreateView,UpdateView,DeleteView)
from django.core.urlresolvers import reverse_lazy
from .models import Movie
from .forms import MovieForm

# class MyView(View)
# 	def get(self,request)
# 		return HttpResponse('result')

class MovieList(ListView):
	template_name = 'movies/movie_list.html'
	model = Movie
	paginate_by = 10
	ordering = ['-year_of_release']

class MovieDetail(DetailView):
	template_name = 'movies/movie_detail.html'
	model = Movie

class MovieCreation(CreateView):
	template_name = 'movies/movie_form.html'
	form_class = MovieForm
	model = Movie
	success_url = reverse_lazy('movies:list')
	queryset = Movie.objects.all()

	def form_valid(self,form):
		print(form.cleaned_data)
		return super().form_valid(form)

class MovieUpdate(UpdateView):
	model = Movie
	success_url = reverse_lazy('movies:list')
	fields = ['name','release_date','genre']

class MovieDelete(DeleteView):
	model = Movie
	success_url = reverse_lazy('movies:list')

